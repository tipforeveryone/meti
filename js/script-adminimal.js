(function($) {
    Drupal.behaviors.myBehavior = {
        attach: function (context, settings) {

            //CODE STARTS HERE

            $("#block-system-navigation h2").click(function(){
                $("#block-system-navigation .content > ul").toggle();
                $(window).scrollTop(0);
            });

            //CODE ENDS HERE
        }
    };
})
(jQuery);
