(function($) {
    Drupal.behaviors.myBehavior = {
        attach: function (context, settings) {

            //FUNCTIONS

                /*LIST OF FUNCTIONS
                responsive_slideshow(classname,original_img_width,original_img_height);
                counter_stylist(id_of_li_element)
                scroll_fading_show_up(element,speed,delay,offset)
                */

                //Responsive Slideshow
                function responsive_slideshow(
                    classname, //Tên class của block slideshow
                    original_img_width, //Chiều rộng gốc của hình sử dụng trong slideshow
                    original_img_height //Chiều cao gốc của hình sử dụng trong slideshow
                ){
                    //Xác định chiều cao của slideshow (responsive) dựa trên tỉ lệ định trước của hình slide
                    var image_ratio = original_img_width / original_img_height;
                    var control_button_height = $(classname + ' .views-slideshow-controls-text-next').height();
                    if($(window).width() < 992){
                        var fieldset_height = $(classname + ' .views-fieldset').height();
                        var slide_height = $(classname).parent().width() / image_ratio + fieldset_height;
                        var control_button_top = (slide_height - fieldset_height) / 2 - control_button_height / 2;
                    }else{
                        var slide_height = $(classname).parent().width() / image_ratio;
                        var control_button_top = slide_height / 2 - control_button_height / 2;
                    }
                    //Định vị lại các thành phần của sliedshow
                    $(classname + ' .views-slideshow-cycle-main-frame').css("height",slide_height);
                    $(classname + ' .views-slideshow-controls-text-next').css("top",control_button_top);
                    $(classname + ' .views-slideshow-controls-text-previous').css("top",control_button_top);
                    $(classname + ' .views-slideshow-pager-fields').css("top",slide_height - 105);
                }

                //Visitor module counter stylist
                function counter_stylist(id_of_li_element){
                    var str0 = $('.visitor > ul > li').eq(id_of_li_element).text();
                    var str1 = str0.slice(0,str0.indexOf(":") + 1);
                    var str2 = str0.slice(str0.indexOf(":") + 2,str0.length);
                    //Fill up with zero numbers
                    if(str2.length < 7){
                        var group_of_zero = "0";
                        var loop_time = 7 - str2.length;
                        for(var x=1; x < loop_time; x++){
                            group_of_zero = group_of_zero + "0"
                        }
                        str2 = group_of_zero + str2;
                    }
                    $('.visitor > ul > li').eq(id_of_li_element).html(
                        "<div class='counter-label'>" + str1 + "</div>" +
                        "<div class='counter-number'>" + str2 + "</div>"
                    );
                }

                //Fading Up element one by one when window scroll reaches the first element offset
                function scroll_fading_show_up(
                    element, //element: $(".selector")
                    speed, //speed: speed of animation
                    delay, //delay: delay between elements, one by one
                    offset //offset: margin top of animated element after animation
                ){

                    var elementTop = element.offset();
                    var elementHeight = element.height();
                    var elementCount = element.length;
                    var animated = false;
                    element.css("margin-top","120px").css("opacity","0"); //Hide element after getting all parameters
                    $(window).scroll(function(){
                        if($(this).scrollTop() > elementTop.top - $(window).height() * 1.2 && animated == false){
                            for(x = 0 ; x < elementCount ; x++){
                                element.eq(x).stop().delay(x * delay).animate({opacity:1,marginTop:0 + offset},speed);
                            }
                            animated = true;
                        }
                    });
                }

            //END OFF FUNCTIONS

            //CODE STARTS HERE

                //Tính toán chiều cao cho slideshow responsive
                responsive_slideshow(".home-slideshow",1920,792);
                $(window).resize(function(){
                    responsive_slideshow(".home-slideshow",1920,792);
                });

                //Tự điền thông tin Tên khóa học cần đăng ký vào form
                var tenkhoahoc = $('.node-content-khoahoc .noidung .field-name-title h2').html();
                //alert(tenkhoahoc);
                $('.formdangkykhoahoc .webform-component--khoahoc .form-text').attr("value","Đăng ký: " + tenkhoahoc);

                // Form đăng ký nhận thông tin
                $(".mailsub form .form-item").wrapAll("<div class='allinput'></div>");

            //CODE ENDS HERE
        }
    };
})
(jQuery);
